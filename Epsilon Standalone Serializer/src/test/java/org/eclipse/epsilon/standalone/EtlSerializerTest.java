package org.eclipse.epsilon.standalone;

import org.eclipse.epsilon.etl.EtlModule;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;

public class EtlSerializerTest {


    private EtlModule etlTree;

    @Before
    public void setup() {
        etlTree = new EtlModule();

    }

    @After
    public void teardown() {
        etlTree.getContext().getModelRepository().dispose();
        etlTree.getContext().dispose();
    }

    @Test
    public void testSerializer() throws Exception {
        etlTree .parse(new File("src/test/resources/Tree2Graph.etl"));
        EtlSerializer serializer = new EtlSerializer(etlTree);
        serializer.print();

    }

    @Test
    public void testParameters() throws Exception {
        etlTree .parse(new File("src/test/resources/F1toBetting.etl"));
        EtlSerializer serializer = new EtlSerializer(etlTree);
        serializer.print();

    }

}
